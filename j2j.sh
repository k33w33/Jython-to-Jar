#!/usr/bin/env bash
output_jar="out.jar"
packaging_files_dir=".packaging_files"

usage() {
  echo "Usage: ./j2j.sh [-o OUTPUT_JAR_NAME.jar]";
}

exists_dependency() {
  local dependency=$1
  
  # check if the path to the file or directory exists
  if [ ! -d $dependency ] && [ ! -f $dependency ]; then
    false
  else
    true
  fi
}

all_dependencies_exist() {
  local missing_dependency=false
  dependencies=($packaging_files_dir/jythonlib.jar \
                $packaging_files_dir/Lib/ \
                $packaging_files_dir/org/python/util/JarRunner.java \
                $packaging_files_dir/META-INF/MANIFEST.MF \
                your_python_stuff/__run__.py)

  # verify the relative path of each dependency exists
  for dependency in "${dependencies[@]}"
  do
    if ! exists_dependency $dependency; then
      missing_dependency=true
      echo "[ERROR] $dependency doesn't exist." >&2
    fi
  done
 
  # return false if a dependency is missing
  if $missing_dependency; then
    false
  else
    true
  fi
}

package_jar() {
  # copy packaging files to top level directory
  cp $packaging_files_dir/jythonlib.jar $output_jar 
  cp -R $packaging_files_dir/Lib . 
  
  # copy everything in your_python_stuff to Lib/ except __run__.py 
  cp -R your_python_stuff/* Lib/
  rm -f Lib/__run__.py
  
  cp -R $packaging_files_dir/org . 
  cp -R $packaging_files_dir/META-INF . 
  cp your_python_stuff/__run__.py .
  
  # jar up the dependencies
  jar uf $output_jar Lib/ 
  jar uf $output_jar org/ 
  jar uf $output_jar __run__.py 
  
  # remove the default MANIFEST.MF generated
  zip -d $output_jar META-INF/MANIFEST.MF > /dev/null 
  
  # add the correct MANIFEST.MF to the jar
  jar ufm $output_jar META-INF/MANIFEST.MF 
  
  # clean up
  rm -rf Lib/ 
  rm -rf org/ 
  rm -rf META-INF/ 
  rm -rf __run__.py 
}

# argument parser
options=':o:h'
while getopts $options option; 
do
  case $option in
    o ) output_jar=("$OPTARG");; # optionally provicde -o flag to specify name of output jar
    h ) usage; exit;; # print help and exit
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
    : ) usage; exit;;
    * ) echo "Invalid option: -$OPTARG" >&2; exit 1;;
  esac
done

# exit if not all dependencies exist
if ! all_dependencies_exist; then
  echo
  echo "Please ensure you are not missing the dependencies printed above." >&2
  echo "Exiting..." >&2
  exit 1
fi

# create the jar
echo "Packaging $output_jar..."
package_jar
echo "Finished! :-)"
exit 0
