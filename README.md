# Jython-to-Jar

A bash script that packages a Jython project into a single runnable jar file. Using Jython 2.7.0.

## How to Use

1. Put your Python scripts and any additional libraries or files that your project uses into the `your_python_stuff` directory.
2. Rename the Python script that contains the main method to `__run__.py`.
3. In `__run__.py` ensure that you remove the `__name__ == '__main__'` check.
4. Navigate to the same directory as `j2j.sh` and run the command `./j2j.sh` which generates the `out.jar` file or you can use `j2j.sh -o OUTPUT_JAR_NAME.jar` to specify the name of the generated jar.

## Using a different version of Jython

If you would like to package your project using a different version of Jython, simply replace the `jythonlib.jar` file  and `Lib` directory in `.packaging_files` with the corresponding file and directory in the other version of Jython. However, you must ensure you rename the default Jython library from `jython.jar` to `jythonlib.jar`.

## Authors

* **Alex Michael** - *Initial work*

## Acknowledgments

* Thanks to zylex over at GitHub for his jython-jar-packager which was used as inspiration for this project.
